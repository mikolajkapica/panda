window.onload = function () {

	var btn = $("button"),
		catDiv = $(".cat"),
		limit = 20,
		xhr = $.get("http://api.giphy.com/v1/gifs/search?q=cats&api_key=4GcI84sihdzA33D3mDQ2L2jIM28pAIpJ&limit=" + limit),
		cats,
		cat;

	btn.on('click', function() {
		cat = cats[Math.floor((Math.random() * limit- 1) )];
		catDiv.css('background-image', 'url(' +  cat.images.original.url + ')');
	});

	xhr.done(function(data) {
		cats = data.data;
		btn.click();
	});
};
