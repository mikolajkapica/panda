window.onload = function () {
	var $j = jQuery.noConflict();


	//              Font Swatch

	var FontSwatch = function (swatch, select) {
		this.swatch = swatch;
		this.select = select;
		this.bindEvents();
	};

	FontSwatch.prototype.bindEvents = function () {
		this.swatch.find('.swatch-item').on('click', this.handleSwatchClick.bind(this));
	};

	FontSwatch.prototype.handleSwatchClick = function (e) {
		var target = $j(e.currentTarget);
		if (this.setSelectValue(target.data('value'))) {
			this.select.trigger('change');
			this.addSelectedClass(target);
		}
	};

	FontSwatch.prototype.setSelectValue = function (value) {
		return this.select.val(value).length === 1;
	};

	FontSwatch.prototype.addSelectedClass = function (selectedItem) {
		this.swatch.children().removeClass('selected');
		selectedItem.addClass('selected');
	};

	fontSwatch = new FontSwatch($j('.swatch'), $j('.font-select'))
}